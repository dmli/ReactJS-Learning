var express = require("express");
var bodyParser = require("body-parser");
var mongoose = require("mongoose");
var path = require("path");
var Webm = require("./data/webm");

//controllers
var webmController = require("./controllers/webmController");
var router = express.Router();

//Express request pipeline
var app = express();
app.use(express.static(path.join(__dirname, "./public/")));
app.use(bodyParser.json())
app.use('/api', webmController);

// middleware to use for all requests
router.use(function(req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

Webm.remove({}, function(err) { 
   console.log('collection removed') 
});

router.route('../webm')
/*
    .post(function(req, res) {

        var webm = new Webm();      
        webm.name = req.body.name; 

        webm.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Webm created!' });
        })

    })*/

    .get(function(req, res) {
        Webm.find(function(err, webm) {
            if (err)
                res.send(err);

            res.json(webm);
        });

    });


app.use('/webm', router);


app.listen(7777, function () {
    console.log("Random webm site launched.");
    console.log("Started listening on port", 7777);
});

// Connect to mongodb database
mongoose.connect("mongodb://localhost/randomwebm");

var fs = require('fs');
 
var path = "./webm";
var items;
var webms = [];
 
fs.readdir(path, function(err, items) {
    //console.log(items);
    
    
    items.forEach(function(element) {
        var webm = new Webm();
        webm.name = element;
        console.log(webm.name);
        webm.save(function(err) {
            if (err)
                console.log(err);
        })
    });
});

function randomWebm() {
    fs.readdir(path, function(err, items) {
            item = items[Math.floor(Math.random()*items.length)];
            return item;
    });
}
