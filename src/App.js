import React, { Component } from 'react';
import background from './images/forest.jpg';
import './App.css';
import './animate.css';
import axios from 'axios';
import Title from './components/Title.js';

let items = [];
let prevItems = [];
let nextItems = [];
let current;
let outOfVids = false;

axios.get('http://147.135.208.92:7777/webm/webm')
    .then(function (response) {
        items = response;
        console.log(items.data.length + ' videos found.');
    })
    .catch(function (error) {
        console.log(error);
    });

function randomWebm() {
    if (items.data.length === 0) {
        outOfVids = true;
        return;
    }
    if (current) {
        prevItems.push(current);
    }
    let rand = Math.floor(Math.random() * items.data.length);
    let webm = items.data[rand].name;
    current = './webm/' + webm;
    console.log(current);
    items.data.splice(rand, 1);
}

const divStyle = {
    backgroundImage: 'url(' + background + ')'
};

class App extends Component {
    constructor() {
        super();

        this.state = {
            clicked: false,
            prev: false
        }

        this.handleClick = this.handleClick.bind(this);
        this._handleRefresh = this._handleRefresh.bind(this);
    }

    componentWillMount() {
        document.addEventListener("keydown", this._handleKeyDown.bind(this));
    }

    _handleRefresh() {
        this.setState({
            clicked: !this.state.clicked
        });
        if (this.state.prev) {
            this.setState({
                clicked: !this.state.clicked,
                prev: false
            });
        }
        else {
            if (nextItems.length === 0) {
                randomWebm();
            }
            else {
                prevItems.push(current);
                current = nextItems.pop();
            }
            this.setState({
                clicked: !this.state.clicked
            });
        }
    }

    _handleKeyDown(event) {
        switch (event.keyCode) {
            case 37:
                if (this.state.clicked) {
                    if (prevItems.length === 0) {
                        break;
                    }
                    nextItems.push(current);
                    current = prevItems.pop();
                    this.setState({
                        prev: true
                    });
                    this._handleRefresh();
                }
                break;
            case 39:
                if (this.state.clicked) {
                    this._handleRefresh();
                }
                break;
            default:
                break;
        }
    }

    handleClick() {
        if (!this.state.clicked && !this.state.prev) {
            randomWebm();
        }
        this.setState({
            clicked: !this.state.clicked
        });
    }

    render() {
        return (
            <div className="App" style={divStyle} >
                {this.state.clicked ? <div id="VideoModal" className="modal" onClick={this.handleClick} ></div> : null}
                {this.state.clicked ? <VideoModal video={current} /> : null}
                <div className="title" >
                    {this.state.clicked ? null : <Title />}
                </div>
                <div className="button animated fadeIn">
                    {outOfVids ?  <p onClick={this.handleClick}>Out of videos</p> : <p onClick={this.handleClick}>HIT ME</p>}
                </div>
                {!this.state.clicked ? 
                null : <div className="footer">                   
                    <p>Navigate videos with arrow keys.</p>
                </div>}
            </div>
        );
    }
}

class VideoModal extends Component {
    constructor() {
        super();

        this.state = {
            webm: { current },
            isSubs: false,
            subs: "null"
        }
    }

    componentWillMount() {
        let subtitles = this.state.webm.current.slice(6, this.state.webm.current.length);
        let pos = subtitles.lastIndexOf(".");
        subtitles = subtitles.slice(0, pos);
        subtitles = '/subs' + subtitles + '.vtt';
        this._handleSubs(subtitles);
    }

    _handleSubs(subtitles) {
        if (subtitles) {
            this.setState({
                isSubs: true,
                subs: subtitles
            }, function () {
                //console.log(this.state.subs);
            });
        }
    }

    render() {
        return (
            <div className="video-player">
                <video controls>
                    <source src={this.state.webm.current} type="video/webm" />
                    {this.state.isSubs ? <track kind="subtitles" src={this.state.subs} srcLang="en" default /> : null}
                </video>
            </div>
        );
    }
}

export default App;
