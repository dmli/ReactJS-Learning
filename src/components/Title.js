import React, { Component } from 'react';
import ReactCSSTransitionGroup from 'react-transition-group/CSSTransitionGroup';
import './Title.css';

class Title extends Component {
    constructor() {
        super();
        this.state = {
            count: 0,
            quote: '',
            nextQuote: '',
            switch: false
        }
    }

    componentWillUnmount() {
        clearInterval(this.timer)
    }

    componentWillMount() {
        this.handleQuote();
    }

    tick() {
        this.setState({ count: (this.state.count + 1) })
        if (this.state.count === 15) {
            this.handleQuote();
            this.setState({
                count: 0,
                switch: !this.state.switch
            });
        }
    }

    handleQuote() {
        let quotes = [
            "Top tier Gondola webms only",
            "Educating the mind without educating the heart is no education at all",
            "The aim of art is to represent not the outward appearance of things, but their inward significance",
            "To read without reflecting is like eating without digesting",
            "The reason angels can fly is because they take themselves lightly",
            "The traveler sees what he sees, the tourist sees what he has come to see",
            "You are now safely wrapped in eternity",
	    "The world will never starve for want of wonders; but only for want of wonder"

        ];
        let rand = Math.floor(Math.random() * quotes.length);
        if (quotes[rand] === this.state.quote || quotes[rand] === this.state.nextQuote) {
            this.handleQuote();
        }
        if (this.state.switch) {
            this.setState({
            nextQuote: (quotes[rand])
        });
        }
        else {
            this.setState({
            quote: (quotes[rand])
        });
        }
    }

    render() {
        return (
            <ReactCSSTransitionGroup transitionName="example" transitionAppear={true} transitionAppearTimeout={1500} transitionEnterTimeout={1500} transitionLeaveTimeout={1000}>
                {this.state.switch ? <Quote2 key={this.state.quote} quote={this.state.quote} /> : <Quote3 key={this.state.nextQuote} quote={this.state.nextQuote} />}
            </ReactCSSTransitionGroup>
        )
    }

    componentDidMount() {
        this.handleQuote();
        this.setState({
            switch: true
        });
        clearInterval(this.timer)
        this.timer = setInterval(this.tick.bind(this), 1000)
    }
}

class Quote2 extends React.Component {
    render() {
        return (
            <h1 className='effect1'>{this.props.quote}</h1>
        );
    }
}

class Quote3 extends React.Component {
    render() {
        return (
            <h1 className='effect2'>{this.props.quote}</h1>
        );
    }
}

export default Title;
